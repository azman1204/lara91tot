@extends('master')
@section('content')

<h1>User Profile</h1>
Name : {{ $name }}
<br>
Address : {{ $addr }}
<br>

<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>No.</th>
            <th>User ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $person)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $person->user_id }}</td>
            <td>{{ $person->name}}</td>
            <td>{{ $person->email }}</td>
            <td>
                <a href="person/edit/{{ $person->user_id }}" class="btn btn-primary">Edit</a>
                <a href="person/delete/{{ $person->user_id }}" class="btn btn-danger">Delete</a><br>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
