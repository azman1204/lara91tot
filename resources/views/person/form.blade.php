@extends('master')
@section('content')

<form action="{{ url("/person/save") }}" method="post">
    @csrf
    User ID : <input type="text" name="user_id" value="{{ $person->user_id }}">
    <br>
    Name : <input type="text" name="name" value="{{ $person->name }}">
    <br>
    Email : <input type="text" name="email" value="{{ $person->email }}">
    <br>
    <input type="submit" value="Save">
</form>

@endsection
