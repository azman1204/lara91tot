<?php
use App\Http\Controllers\PersonController;
use Illuminate\Support\Facades\Route;

// http://lara9tot.test/profile
//Route::get('/profile', 'PersonController@profile'); // laravel version < 8
Route::get('/profile', [PersonController::class, 'profile']); // laravel 8 dan 9
Route::get('/create', [PersonController::class, 'create']);
Route::post('/person/save', [PersonController::class, 'save']);
Route::get('/person/edit/{user_id}', [PersonController::class, 'edit']);
Route::get('/person/delete/{user_id}', [PersonController::class, 'delete']);

// http://lara9tot.test/
Route::get('/', function () {
    return view('welcome'); // resources/views/welcome.blade.php
});

// http://lara9tot.test/hello.html
Route::get('hello.html', function() {
    return view('hello');// resources/views/hello.blade.php
});


