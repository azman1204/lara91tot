<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Person;

class PersonController extends Controller
{
    function profile() {
        // resources/views/person/profile.blade.php
        $arr = ['name' => 'azman', 'addr' => 'Bangi'];
        // passing data from controller to view
        // ctl + p = search file
        $data = Person::all(); // select * from person
        // $data = array of object Person
        $arr['data'] = $data;
        return view('person/profile', $arr);
    }

    function create() {
        $person = new Person();
        $arr = ['person' => $person];
        return view('person/form', $arr);
    }

    function save(Request $req) {
        $person = Person::where('user_id', $req->user_id)->first();
        // ! = not
        if (! $person) {
            // data tidak wujud
            $person = new Person();
        }
        // read data from form
        $person->user_id = $req->user_id;
        $person->name = $req->name;
        $person->email = $req->email;
        $person->save();
        return redirect('profile');
    }

    function edit($user_id) {
        //echo $user_id;
        // select * from users where user_id = 1001
        // return an object
        $person = Person::where('user_id', $user_id)->first();
        $arr = ['person' => $person];
        return view('person/form', $arr);
    }

    function delete($user_id) {
        // cari by PK
        Person::find($user_id)->delete();
        return redirect('profile');
    }
}
