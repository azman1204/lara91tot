<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Person extends Model {
    // laravel assume semua table ada PK id int
    use HasFactory;
    //public $connection = 'mysql2';
    public $table = 'users';
    public $primaryKey = 'user_id'; // tambah ini hanya jika primary key bukan "id"
    // ini adalah nama table dlm DB php_training
}
